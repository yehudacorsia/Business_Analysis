

var resultToShow = {
  "root": {
    "creditCheck": {
      "ratiosList": {
        "ratioEntry": [
          {
            "ratioType": "profitAndLoss",
            "ratioEntryDescription": "דוח רווח והפסד",
            "ratioEntryFieldsList": {
              "ratioEntryFieldEntry": [
                {
                  "fieldType": "1000",
                  "value": "3.10171303658e+11",
                  "stringValue": "310,171,303,658",
                  "unit": "₪",
                  "description": "הכנסות ממכירות, מתן שירותים וביצוע עבודות"
                },
                {
                  "fieldType": "1300",
                  "value": "62230395527.0",
                  "stringValue": "62,230,395,527",
                  "unit": "₪",
                  "description": "עלות המכירות, מתן שירותים וביצוע עבודות"
                },
                {
                  "fieldType": "2000",
                  "value": "3513513515.0",
                  "stringValue": "3,513,513,515",
                  "unit": "₪",
                  "description": "עלויות ייצור ועלויות אצל קבלן בונה"
                },
                {
                  "fieldType": "3000",
                  "value": "125615.0",
                  "stringValue": "125,615",
                  "unit": "₪",
                  "description": "הוצאות מכירה"
                },
                {
                  "fieldType": "3500",
                  "value": "5135135.0",
                  "stringValue": "5,135,135",
                  "unit": "₪",
                  "description": "הוצאות הנהלה וכלליות"
                },
                {
                  "fieldType": "5000",
                  "value": "153145341.0",
                  "stringValue": "153,145,341",
                  "unit": "₪",
                  "description": "הוצאות מימון"
                },
                {
                  "fieldType": "5100",
                  "value": "1513516.0",
                  "stringValue": "1,513,516",
                  "unit": "₪",
                  "description": "הכנסות מימון"
                },
                {
                  "fieldType": "5200",
                  "value": "5113229.0",
                  "stringValue": "5,113,229",
                  "unit": "₪",
                  "description": "הכנסות אחרות"
                },
                {
                  "fieldType": "5300",
                  "value": "1351513515.0",
                  "stringValue": "1,351,513,515",
                  "unit": "₪",
                  "description": "הוצאות אחרות"
                },
                {
                  "fieldType": "1300%",
                  "value": "0.20063234346",
                  "stringValue": "20",
                  "unit": "%",
                  "description": "עלות המכירות, מתן שירותים וביצוע עבודות"
                },
                {
                  "fieldType": "2000%",
                  "value": "0.0113276549879",
                  "stringValue": "1",
                  "unit": "%",
                  "description": "עלויות ייצור ועלויות אצל קבלן בונה"
                },
                {
                  "fieldType": "3000%",
                  "value": "4.04985885279e-07",
                  "stringValue": "0",
                  "unit": "%",
                  "description": "הוצאות מכירה"
                },
                {
                  "fieldType": "3500%",
                  "value": "1.65558030012e-05",
                  "stringValue": "0",
                  "unit": "%",
                  "description": "הוצאות הנהלה וכלליות"
                },
                {
                  "fieldType": "5000%",
                  "value": "0.000493744389613",
                  "stringValue": "0",
                  "unit": "%",
                  "description": "הוצאות מימון"
                },
                {
                  "fieldType": "5100%",
                  "value": "4.87961324001e-06",
                  "stringValue": "0",
                  "unit": "%",
                  "description": "הכנסות מימון"
                },
                {
                  "fieldType": "5200%",
                  "value": "1.64851775122e-05",
                  "stringValue": "0",
                  "unit": "%",
                  "description": "הכנסות אחרות"
                },
                {
                  "fieldType": "5300%",
                  "value": "0.00435731319777",
                  "stringValue": "0",
                  "unit": "%",
                  "description": "הוצאות אחרות"
                },
                {
                  "fieldType": "grossProfit",
                  "value": "2.44427394616e+11",
                  "stringValue": "244,427,394,616",
                  "unit": "₪",
                  "description": "רווח גולמי"
                },
                {
                  "fieldType": "ebit",
                  "value": "2.4307573358e+11",
                  "stringValue": "243,075,733,580",
                  "unit": "₪",
                  "description": "רווח תפעולי / EBIT"
                },
                {
                  "fieldType": "profitBeforeTax",
                  "value": "2.42924101755e+11",
                  "stringValue": "242,924,101,755",
                  "unit": "₪",
                  "description": "רווח / הפסד לפני מיסים"
                },
                {
                  "fieldType": "grossProfit%",
                  "value": "0.788040001552",
                  "stringValue": "78",
                  "unit": "%",
                  "description": "רווח גולמי"
                },
                {
                  "fieldType": "ebit%",
                  "value": "0.783682212743",
                  "stringValue": "78",
                  "unit": "%",
                  "description": "רווח תפעולי / EBIT"
                },
                {
                  "fieldType": "profitBeforeTax%",
                  "value": "0.783193347966",
                  "stringValue": "78",
                  "unit": "%",
                  "description": "רווח / הפסד לפני מיסים"
                }
              ]
            }
          },
          {
            "ratioType": "returnCapital",
            "ratioEntryDescription": "הון חוזר",
            "ratioEntryFieldsList": {
              "ratioEntryFieldEntry": [
                {
                  "fieldType": "7300",
                  "value": "256156.0",
                  "stringValue": "256,156",
                  "unit": "₪",
                  "description": "לקוחות"
                },
                {
                  "fieldType": "7410",
                  "value": "156151.0",
                  "stringValue": "156,151",
                  "unit": "₪",
                  "description": "מקדמות לספקים ואחרים"
                },
                {
                  "fieldType": "7800",
                  "value": "1.0",
                  "stringValue": "1",
                  "unit": "₪",
                  "description": "מלאי"
                },
                {
                  "fieldType": "9200",
                  "value": "3.0",
                  "stringValue": "3",
                  "unit": "₪",
                  "description": "ספקים ונותני שרותים"
                },
                {
                  "fieldType": "9370",
                  "value": "35.0",
                  "stringValue": "35",
                  "unit": "₪",
                  "description": "מקדמות מלקוחות"
                },
                {
                  "fieldType": "supply",
                  "value": "1.0",
                  "stringValue": "1",
                  "unit": "₪",
                  "description": "מלאי"
                },
                {
                  "fieldType": "customers",
                  "value": "256121.0",
                  "stringValue": "256,121",
                  "unit": "₪",
                  "description": "לקוחות (בניכוי מקדמות מלקוחות)"
                },
                {
                  "fieldType": "suppliers",
                  "value": "-156148.0",
                  "stringValue": "-156,148",
                  "unit": "₪",
                  "description": "ספקים (בניכוי מקדמות לספקים)"
                },
                {
                  "fieldType": "returnCapital",
                  "value": "412270.0",
                  "stringValue": "412,270",
                  "unit": "₪",
                  "description": "הון חוזר"
                }
              ]
            }
          },
          {
            "ratioType": "timeGap",
            "ratioEntryDescription": "פער הזמן בימים",
            "ratioEntryFieldsList": {
              "ratioEntryFieldEntry": [
                {
                  "fieldType": "1000",
                  "value": "3.10171303658e+11",
                  "stringValue": "310,171,303,658",
                  "unit": "₪",
                  "description": "הכנסות ממכירות, מתן שירותים וביצוע עבודות"
                },
                {
                  "fieldType": "1300",
                  "value": "62230395527.0",
                  "stringValue": "62,230,395,527",
                  "unit": "₪",
                  "description": "עלות המחירות, מתן השרותים וביצוע העבודות"
                },
                {
                  "fieldType": "2000",
                  "value": "3513513515.0",
                  "stringValue": "3,513,513,515",
                  "unit": "₪",
                  "description": "עלויות ייצור ועלויות אצל קבלן בונה"
                },
                {
                  "fieldType": "7300",
                  "value": "256156.0",
                  "stringValue": "256,156",
                  "unit": "₪",
                  "description": "לקוחות"
                },
                {
                  "fieldType": "7800",
                  "value": "1.0",
                  "stringValue": "1",
                  "unit": "₪",
                  "description": "מלאי"
                },
                {
                  "fieldType": "9200",
                  "value": "3.0",
                  "stringValue": "3",
                  "unit": "₪",
                  "description": "ספקים ונותני שרותים"
                },
                {
                  "fieldType": "shoppingFromSuppliers",
                  "value": "5265188138.0",
                  "stringValue": "5,265,188,138",
                  "unit": "₪"
                },
                {
                  "fieldType": "supplyDays",
                  "value": "5.55184511111e-09",
                  "stringValue": "0",
                  "unit": "₪",
                  "description": "ימי החזקת מלאי"
                },
                {
                  "fieldType": "customersDays",
                  "value": "0.000301436460747",
                  "stringValue": "0",
                  "unit": "₪",
                  "description": "ימי אשראי לקוחות"
                },
                {
                  "fieldType": "suppliersDays",
                  "value": "2.07969776445e-07",
                  "stringValue": "0",
                  "unit": "₪",
                  "description": "ימי אשראי ספקים"
                },
                {
                  "fieldType": "timeGap",
                  "value": "0.000301234042816",
                  "stringValue": "0",
                  "unit": "₪",
                  "description": "פער הזמן בימים"
                },
                {
                  "fieldType": "operatingWorkingCapital",
                  "value": "255983.988401",
                  "stringValue": "255,983",
                  "unit": "₪",
                  "description": "הון חוזר תפעולי"
                }
              ]
            }
          },
          {
            "ratioType": "altmanModel",
            "ratioEntryDescription": "מודל אלטמן",
            "ratioEntryFieldsList": {
              "ratioEntryFieldEntry": [
                {
                  "fieldType": "1000",
                  "value": "3.10171303658e+11",
                  "stringValue": "310,171,303,658",
                  "unit": "₪",
                  "description": "הכנסות ממכירות, מתן שירותים וביצוע עבודות"
                },
                {
                  "fieldType": "7000",
                  "value": "531.0",
                  "stringValue": "531",
                  "unit": "₪",
                  "description": "נכסים שוטפים/רכוש שוטף"
                },
                {
                  "fieldType": "9000",
                  "value": "135.0",
                  "stringValue": "135",
                  "unit": "₪",
                  "description": "התחייבויות שוטפות"
                },
                {
                  "fieldType": "9600",
                  "value": "63531.0",
                  "stringValue": "63,531",
                  "unit": "₪",
                  "description": "התחייבויות לזמן ארוך"
                },
                {
                  "fieldType": "9700",
                  "value": "51.0",
                  "stringValue": "51",
                  "unit": "₪",
                  "description": "התחייבויות בשל סיום יחסי עובד מעביד"
                },
                {
                  "fieldType": "9800",
                  "value": "351.0",
                  "stringValue": "351",
                  "unit": "₪",
                  "description": "עתודה למיסים נדחים לזמן ארוך"
                },
                {
                  "fieldType": "9900",
                  "value": "5150351053.0",
                  "stringValue": "5,150,351,053",
                  "unit": "₪",
                  "description": "הון עצמי"
                },
                {
                  "fieldType": "9980",
                  "value": "153103515.0",
                  "stringValue": "153,103,515",
                  "unit": "₪",
                  "description": "רווח/הפסד שנצבר (עודפים)"
                },
                {
                  "fieldType": "9999",
                  "value": "13103510515.0",
                  "stringValue": "13,103,510,515",
                  "unit": "₪",
                  "description": "סה\"כ התחייבויות והון"
                },
                {
                  "fieldType": "ebit",
                  "value": "2.4307573358e+11",
                  "stringValue": "243,075,733,580",
                  "unit": "₪",
                  "description": "רווח תפעולי / EBIT"
                },
                {
                  "fieldType": "netReturnCapital",
                  "value": "396.0",
                  "stringValue": "396",
                  "unit": "₪",
                  "description": "הון חוזר נטו"
                },
                {
                  "fieldType": "foreignCapital",
                  "value": "13103574583.0",
                  "stringValue": "13,103,574,583",
                  "unit": "₪",
                  "description": "הון זר"
                },
                {
                  "fieldType": "alpha1",
                  "value": "3.02209090874e-08",
                  "stringValue": "0",
                  "unit": "₪",
                  "description": "a1"
                },
                {
                  "fieldType": "alpha2",
                  "value": "0.0116841601207",
                  "stringValue": "0",
                  "unit": "₪",
                  "description": "a2"
                },
                {
                  "fieldType": "alpha3",
                  "value": "18.5504283987",
                  "stringValue": "18",
                  "unit": "₪",
                  "description": "a3"
                },
                {
                  "fieldType": "alpha4",
                  "value": "0.393049317984",
                  "stringValue": "0",
                  "unit": "₪",
                  "description": "a4"
                },
                {
                  "fieldType": "alpha5",
                  "value": "23.6708554782",
                  "stringValue": "23",
                  "unit": "₪",
                  "description": "a5"
                },
                {
                  "fieldType": "z",
                  "value": "85.1394566451",
                  "stringValue": "85",
                  "unit": "₪",
                  "description": "מודל אלטמן"
                },
                {
                  "fieldType": "zone",
                  "value": "2",
                  "stringValue": "2",
                  "description": "החברה נמצאת באזור הבטוח - אין סכנה לחדלות פירעון"
                }
              ]
            }
          },
          {
            "ratioType": "deptServiceRatio",
            "ratioEntryDescription": "יחס שירות חוב",
            "ratioEntryFieldsList": {
              "ratioEntryFieldEntry": [
                {
                  "fieldType": "2080",
                  "value": "965135.0",
                  "stringValue": "965,135",
                  "unit": "₪",
                  "description": "פחת עלויות ייצור ועלויות אצל קבלן בונה"
                },
                {
                  "fieldType": "3080",
                  "value": "165310351.0",
                  "stringValue": "165,310,351",
                  "unit": "₪",
                  "description": "פחת הוצאות מכירה"
                },
                {
                  "fieldType": "3580",
                  "value": "845354.0",
                  "stringValue": "845,354",
                  "unit": "₪",
                  "description": "פחת הוצאות הנאלה וכלליות"
                },
                {
                  "fieldType": "5000",
                  "value": "153145341.0",
                  "stringValue": "153,145,341",
                  "unit": "₪",
                  "description": "הוצאות מימון"
                },
                {
                  "fieldType": "5100",
                  "value": "1513516.0",
                  "stringValue": "1,513,516",
                  "unit": "₪",
                  "description": "הכנסות מימון"
                },
                {
                  "fieldType": "9130",
                  "value": "13510351.0",
                  "stringValue": "13,510,351",
                  "unit": "₪",
                  "description": "חלויות שוטפות של הלוואות לזמן ארוך"
                },
                {
                  "fieldType": "ebit",
                  "value": "2.4307573358e+11",
                  "stringValue": "243,075,733,580",
                  "unit": "₪",
                  "description": "רווח תפעולי / EBIT"
                },
                {
                  "fieldType": "da",
                  "value": "167120840.0",
                  "stringValue": "167,120,840",
                  "unit": "₪",
                  "description": "פחת"
                },
                {
                  "fieldType": "ebita",
                  "value": "2.4324285442e+11",
                  "stringValue": "243,242,854,420",
                  "unit": "₪",
                  "description": "EBITA"
                },
                {
                  "fieldType": "finnacialExpenses",
                  "value": "151631825.0",
                  "stringValue": "151,631,825",
                  "unit": "₪",
                  "description": "הוצאות מימון"
                },
                {
                  "fieldType": "currentMaturity",
                  "value": "13510351.0",
                  "stringValue": "13,510,351",
                  "unit": "₪",
                  "description": "חלות שוטפת"
                },
                {
                  "fieldType": "annualPaymantAmount",
                  "value": "165142176.0",
                  "stringValue": "165,142,176",
                  "unit": "₪",
                  "description": "סכום החזר תשלום שנתי"
                },
                {
                  "fieldType": "deptServiceRatio",
                  "value": "1472.92993414",
                  "stringValue": "1,472",
                  "unit": "₪",
                  "description": "יחס שירות חוב"
                }
              ]
            }
          },
          {
            "ratioType": "activityTargeting",
            "ratioEntryDescription": "נכסי מיקוד פעילות",
            "ratioEntryFieldsList": {
              "ratioEntryFieldEntry": [
                {
                  "fieldType": "7100",
                  "value": "351351351.0",
                  "stringValue": "351,351,351",
                  "unit": "₪",
                  "description": "מזומנים ושווי מזומנים"
                },
                {
                  "fieldType": "7300",
                  "value": "256156.0",
                  "stringValue": "256,156",
                  "unit": "₪",
                  "description": "לקוחות"
                },
                {
                  "fieldType": "7800",
                  "value": "1.0",
                  "stringValue": "1",
                  "unit": "₪",
                  "description": "מלאי"
                },
                {
                  "fieldType": "8000",
                  "value": "74654.0",
                  "stringValue": "74,654",
                  "unit": "₪",
                  "description": "רכוש קבוע"
                },
                {
                  "fieldType": "8440",
                  "value": "9541941.0",
                  "stringValue": "9,541,941",
                  "unit": "₪",
                  "description": "השקאות בניירות ערך סדירים"
                },
                {
                  "fieldType": "8600",
                  "value": "135.0",
                  "stringValue": "135",
                  "unit": "₪",
                  "description": "הוצאות נדחות ורכוש אחר"
                },
                {
                  "fieldType": "9999",
                  "value": "13103510515.0",
                  "stringValue": "13,103,510,515",
                  "unit": "₪",
                  "description": "סה\"כ התחייבויות והון"
                },
                {
                  "fieldType": "activeTargetingRatio",
                  "value": "2.52562853001e-05",
                  "stringValue": "0",
                  "unit": "₪",
                  "description": "שיעור מיקוד פעילות"
                },
                {
                  "fieldType": "neutralAssetRatio",
                  "value": "0.0275417256763",
                  "stringValue": "0",
                  "unit": "₪",
                  "description": "שיעור נכס נייטרלי"
                },
                {
                  "fieldType": "overallActivityTargetingRate",
                  "value": "0.0275669819616",
                  "stringValue": "0",
                  "unit": "₪",
                  "description": "שיעור מיקוד פעילות כולל"
                }
              ]
            }
          },
          {
            "ratioType": "personalCapital",
            "ratioEntryDescription": "הון עצמי מתוקן",
            "ratioEntryFieldsList": {
              "ratioEntryFieldEntry": [
                {
                  "fieldType": "7450",
                  "value": "1.0",
                  "stringValue": "1",
                  "unit": "₪",
                  "description": "בעלי מניות (במישרין)"
                },
                {
                  "fieldType": "7461",
                  "value": "2561531.0",
                  "stringValue": "2,561,531",
                  "unit": "₪",
                  "description": "צדדים קשורים בארץ"
                },
                {
                  "fieldType": "7462",
                  "value": "351.0",
                  "stringValue": "351",
                  "unit": "₪",
                  "description": "צדדים קשורים בחול"
                },
                {
                  "fieldType": "8020",
                  "value": "1.0",
                  "stringValue": "1",
                  "unit": "₪",
                  "description": "שיפורים במושכר"
                },
                {
                  "fieldType": "8050",
                  "value": "51.0",
                  "stringValue": "51",
                  "unit": "₪",
                  "description": "מחשבים וציוד עיבוד נתונים"
                },
                {
                  "fieldType": "8120",
                  "value": "5626512.0",
                  "stringValue": "5,626,512",
                  "unit": "₪",
                  "description": "פחת שנצבר - שיפורים במושכר"
                },
                {
                  "fieldType": "8150",
                  "value": "87654153.0",
                  "stringValue": "87,654,153",
                  "unit": "₪",
                  "description": "פחת שנצבר - מחשבים וציוד עיבוד נתונים"
                },
                {
                  "fieldType": "8610",
                  "value": "35.0",
                  "stringValue": "35",
                  "unit": "₪",
                  "description": "מוניטין"
                },
                {
                  "fieldType": "8620",
                  "value": "135.0",
                  "stringValue": "135",
                  "unit": "₪",
                  "description": "הוצאות יסוד"
                },
                {
                  "fieldType": "8640",
                  "value": "351.0",
                  "stringValue": "351",
                  "unit": "₪",
                  "description": "שיערוך נכסים בלתי מוחשיים כנגד קרן הון"
                },
                {
                  "fieldType": "8690",
                  "value": "35.0",
                  "stringValue": "35",
                  "unit": "₪",
                  "description": "זיכיון"
                },
                {
                  "fieldType": "9151",
                  "value": "3510.0",
                  "stringValue": "3,510",
                  "unit": "₪",
                  "description": "הלוואות מצדדים קשורים בארץ - בנקים והלוואות לזמן קצר"
                },
                {
                  "fieldType": "9152",
                  "value": "351.0",
                  "stringValue": "351",
                  "unit": "₪",
                  "description": "הלוואות מצדדים קשורים בחול - בנקים והלוואות לזמן קצר"
                },
                {
                  "fieldType": "9631",
                  "value": "156153053.0",
                  "stringValue": "156,153,053",
                  "unit": "₪",
                  "description": "הלוואות מצדדים קשורים בארץ - התחייבויות לזמן ארוך"
                },
                {
                  "fieldType": "9632",
                  "value": "15610.0",
                  "stringValue": "15,610",
                  "unit": "₪",
                  "description": "הלוואות מצדדים קשורים בחול - התחייבויות לזמן ארוך"
                },
                {
                  "fieldType": "9660",
                  "value": "15.0",
                  "stringValue": "15",
                  "unit": "₪",
                  "description": "שטרי הון"
                },
                {
                  "fieldType": "9900",
                  "value": "5150351053.0",
                  "stringValue": "5,150,351,053",
                  "unit": "₪",
                  "description": "סה\"כ הון עצמי"
                },
                {
                  "fieldType": "9999",
                  "value": "13103510515.0",
                  "stringValue": "13,103,510,515",
                  "unit": "₪",
                  "description": "סה\"כ התחייבויות והון"
                },
                {
                  "fieldType": "foreignCapital",
                  "value": "13103574583.0",
                  "stringValue": "13,103,574,583",
                  "unit": "₪",
                  "description": "הון זר"
                },
                {
                  "fieldType": "pullingOwners",
                  "value": "2561883.0",
                  "stringValue": "2,561,883",
                  "unit": "₪",
                  "description": "משיכת בעלים"
                },
                {
                  "fieldType": "rentalImprovments",
                  "value": "-5626511.0",
                  "stringValue": "-5,626,511",
                  "unit": "₪",
                  "description": "שיפורים במושכר"
                },
                {
                  "fieldType": "comuterSoftware",
                  "value": "-87654102.0",
                  "stringValue": "-87,654,102",
                  "unit": "₪",
                  "description": "תוכנות מחשב"
                },
                {
                  "fieldType": "abc",
                  "value": "556.0",
                  "stringValue": "556",
                  "unit": "₪",
                  "description": "זיכון + מוניטין + שיערוך נכסים בלתי מוחשיים כנגד קרן הון + הוצאות יסוד = ABC"
                },
                {
                  "fieldType": "ownersLoan",
                  "value": "156172539.0",
                  "stringValue": "156,172,539",
                  "unit": "₪",
                  "description": "הלוואות בעלים בהנחת סבורדיניישן"
                },
                {
                  "fieldType": "fixedPersonalCapital",
                  "value": "5397241766.0",
                  "stringValue": "5,397,241,766",
                  "unit": "₪",
                  "description": "הון עצמי מתוקן"
                },
                {
                  "fieldType": "fixedPersonalCapitalRate",
                  "value": "0.291729925004",
                  "stringValue": "0",
                  "unit": "₪",
                  "description": "שיעור הון עצמי מתוקן"
                },
                {
                  "fieldType": "personalCapitalRate",
                  "value": "0.39305123975",
                  "stringValue": "0",
                  "unit": "₪",
                  "description": "שיעור ההון העצמי"
                }
              ]
            }
          }
        ]
      }
    }
  }
};