var topics = resultToShow.root.creditCheck.ratiosList.ratioEntry ;

var app = angular.module('myApp', ["ngRoute"]);
app.controller('myCtrl', function($scope) {
    $scope.firstName= "John";
    $scope.lastName= "Doe";
    $scope.indexDescription;
    
    $scope.descriptionsTopics = [];
    
    $scope.profitForCheck    = resultToShow.root.creditCheck.ratiosList.ratioEntry[0].ratioEntryFieldsList.ratioEntryFieldEntry;
    $scope.returnCapital     = resultToShow.root.creditCheck.ratiosList.ratioEntry[1].ratioEntryFieldsList.ratioEntryFieldEntry;
    $scope.timeGap           = resultToShow.root.creditCheck.ratiosList.ratioEntry[2].ratioEntryFieldsList.ratioEntryFieldEntry;
    $scope.altmanModel       = resultToShow.root.creditCheck.ratiosList.ratioEntry[3].ratioEntryFieldsList.ratioEntryFieldEntry;
    $scope.deptServiceRatio  = resultToShow.root.creditCheck.ratiosList.ratioEntry[4].ratioEntryFieldsList.ratioEntryFieldEntry;
    $scope.activityTargeting = resultToShow.root.creditCheck.ratiosList.ratioEntry[5].ratioEntryFieldsList.ratioEntryFieldEntry;
    $scope.personalCapital   = resultToShow.root.creditCheck.ratiosList.ratioEntry[6].ratioEntryFieldsList.ratioEntryFieldEntry;
    

    for ($scope.indexDescription = 0; $scope.indexDescription < topics.length; $scope.indexDescription++) {
       $scope.descriptionsTopics[$scope.indexDescription] = {"forHref" : "#"  + topics[$scope.indexDescription].ratioType,  "desc" : topics[$scope.indexDescription].ratioEntryDescription};

    }
});


app.config(function($routeProvider) {
    $routeProvider
    .when("/profitAndLoss", {
        templateUrl : "templateUrls/profitAndLoss.html"
    })
    .when("/returnCapital", {
        templateUrl : "templateUrls/returnCapital.html"
    })
    .when("/timeGap", {
        templateUrl : "templateUrls/timeGap.html"
    })
    .when("/altmanModel", {
        templateUrl : "templateUrls/altmanModel.html"
    })
    .when("/deptServiceRatio", {
        templateUrl : "templateUrls/deptServiceRatio.html"
    })
    .when("/activityTargeting", {
        templateUrl : "templateUrls/activityTargeting.html"
    })
    .when("/personalCapital", {
        templateUrl : "templateUrls/personalCapital.html"
    });
});